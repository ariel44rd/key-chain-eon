######README.MD####

steps to build app from XCode

- open terminal from root
- run [ pod install ]
- run [ open Key\ chain.xcworkspace/ ]
- sign in into XCode with provisioning profile & private key alloc @ Key Chain system app
- if you don't have any provisioning profile please contact the developer: ariel44r@gmail.com or simply run project using a simulator as build target

pasos para construir la app
- abrir carpeta contenedora del proyecto
- abrir un terminal
- ingresar comando [ pod install ]
- ingresar comando [ open Key\ chain.xcworkspace/ ]
- firmarse como a la cuenta de desarrollador con provisioning profile y llave privada del certificado de desarrollo previamnte obtenidos
- si no cuentas con ningún certificado por favor contacta al desarrollador: ariel44r@gmail.com o simplemente compila el proyecto usando un simulador como destino de compilación

