//
//  LoginRequest.swift
//  Key chain
//
//  Created by Ariel on 10/20/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import ObjectMapper

class LoginRequest: Mappable {
    var userID: Int!
    var appVersion: String!
    var deviceID: String!
    var token: String!
    var authData: AuthData!

    init(userID: Int, appVersion: String, deviceID: String, token: String, authData: AuthData?) {
        self.userID = userID
        self.appVersion = appVersion
        self.deviceID = deviceID
        self.token = token
        self.authData = authData
        
    }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        userID <- map["IdUsuario"]
        appVersion <- map["VersionApp"]
        deviceID <- map["IdDispositivo"]
        token <- map["Token"]
        authData <- map["ObjetoPrincipal"]

    }
}

class AuthData: Mappable {
    var userName: String!
    var password: String!
    
    init(userName: String, password: String) {
        self.userName = userName
        self.password = password
        
    }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        userName <- map["userName"]
        password <- map["password"]
        
    }
}
