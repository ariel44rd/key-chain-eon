//
//  LoginResponse.swift
//  Key chain
//
//  Created by Ariel on 10/20/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import ObjectMapper

class LoginResponse: BaseResponse {
    var userData: UserData?
    
    required init?(map: Map) {
        super.init(map: map)
        
    }
    override func mapping(map: Map) {
        super.mapping(map: map)
        userData <- map["ObjetoPrincipal"]
        
    }
    
}

class UserData: Mappable {
    var token: String!
    var userName: String!
    var email: String!
    var name: String!
    var lastName: String!
    var mothersLastName: String!
    var birthDay: String!
    var gender: Int!
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        token <- map["token"]
        userName <- map["userName"]
        email <- map["email"]
        name <- map["nombre"]
        lastName <- map["paterno"]
        mothersLastName <- map["materno"]
        birthDay <- map["fechaNacimiento"]
        gender <- map["sexo"]
        
    }
    
    func validate() -> Bool {
        if let _ = self.token,
        let _ = self.userName,
        let _ = self.email,
        let _ = self.name,
        let _ = self.lastName,
        let _ = self.mothersLastName,
        let _ = self.birthDay,
        let _ = self.gender {
            return true
            
        } else {
            return false

        }
    }
    
}
