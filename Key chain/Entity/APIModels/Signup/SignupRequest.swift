//
//  SignupRequest.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import ObjectMapper

class SignupRequest: Mappable {
    var userID: Int!
    var appVersion: String!
    var deviceID: String!
    var token: String!
    var signupData: SignupData!
    
    init(userID: Int, appVersion: String, deviceID: String, token: String, signupData: SignupData?) {
        self.userID = userID
        self.appVersion = appVersion
        self.deviceID = deviceID
        self.token = token
        self.signupData = signupData
        
    }
    required init?(map: Map) { }
    func mapping(map: Map) {
        userID <- map["IdUsuario"]
        appVersion <- map["VersionApp"]
        deviceID <- map["IdDispositivo"]
        token <- map["Token"]
        signupData <- map["ObjetoPrincipal"]
        
    }
}

class SignupData: Mappable {
    var userName: String!
    var email: String!
    var name: String!
    var lastName: String!
    var mothersLastName: String!
    var birthDay: String!
    var gender: Int!
    var password: String!
    
    init(_ userName: String, _ email: String, _ name: String, _ lastName: String, _ mothersLastName: String, birthDay: String, gender: Int, password: String) {
        self.userName = userName
        self.email = email
        self.name = name
        self.lastName = lastName
        self.mothersLastName = mothersLastName
        self.birthDay = birthDay
        self.gender = gender
        self.password = password
        
    }
    required init?(map: Map) { }
    func mapping(map: Map) {
        userName <- map["userName"]
        email <- map["email"]
        name <- map["nombre"]
        lastName <- map["paterno"]
        mothersLastName <- map["materno"]
        birthDay <- map["fechaNacimiento"]
        gender <- map["sexo"]
        password <- map["password"]
        
    }
}
