//
//  BaseResponse.swift
//  Key chain
//
//  Created by Ariel on 10/20/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import ObjectMapper

class BaseResponse: Mappable {
    var errorGuid: String?
    var errorMessage: String?
    
    init(){ }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        errorGuid <- map["ErrorGuid"]
        errorMessage <- map["ErrorMensaje"]
        
    }
}


class AuthenticateResponse: BaseResponse {
    var mainObject: String!
    
    required init?(map: Map) {
        super.init(map: map)
        
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        mainObject <- map["MainObject"]
        
    }
}
