//
//  ForgotPasswordResponse.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import ObjectMapper

class ForgotPasswordResponse: BaseResponse {
    var mainObject: Bool!
    
    required init?(map: Map) {
        super.init(map: map)
        
    }
    override func mapping(map: Map) {
        super.mapping(map: map)
        mainObject <- map["ObjetoPrincipal"]
        
    }
}
