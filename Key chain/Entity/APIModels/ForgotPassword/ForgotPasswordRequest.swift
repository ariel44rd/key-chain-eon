//
//  ForgotPasswordRequest.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import ObjectMapper

class ForgotPasswordRequest: Mappable {
    var userID: Int!
    var appVersion: String!
    var deviceID: String!
    var token: String!
    var forgotPasswordData: ForgotPasswordData!
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        userID <- map["IdUsuario"]
        appVersion <- map["VersionApp"]
        deviceID <- map["IdDispositivo"]
        token <- map["Token"]
        forgotPasswordData <- map["ObjetoPrincipal"]
        
    }
}

class ForgotPasswordData: Mappable {
    var userName: String!
    var newPassword: String!
    var email: String!
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        userName <- map["userName"]
        newPassword <- map["NewPassword"]
        email <- map["email"]
        
    }
}
