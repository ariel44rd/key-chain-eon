//
//  TrapezoidalFrameView.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

@IBDesignable
class TrapezoidalFrameView: UIView {

    // MARK: Attributes
    var bezierPath: UIBezierPath!
    @IBInspectable var color: UIColor!
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.color = .eonColor(.bgBlue)
        let height: CGFloat = self.frame.height
        let width: CGFloat = self.frame.width
        self.bezierPath = UIBezierPath()
        self.makeViewWith(features: [.color(.clear)])
        self.bezierPath.move(to: CGPoint(x: 0, y: 0))
        self.bezierPath.addLine(to: CGPoint(x: width, y: 0))
        self.bezierPath.addLine(to: CGPoint(x: width, y: height/1.2))
        self.bezierPath.addLine(to: CGPoint(x: 0, y: height))
        self.color.setFill()
        bezierPath.fill()
        bezierPath.stroke()
        
    }

}
