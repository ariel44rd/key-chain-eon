//
//  RateView.swift
//  Key chain
//
//  Created by Ariel on 10/22/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

protocol RateViewDelegate {
    func rateSelected(index: Int)
    
}

class RateView: UIView {
    
    // MARK: Attributer
    var starOne: UIButton!
    var starTwo: UIButton!
    var starthree: UIButton!
    var starFour: UIButton!
    var starFive: UIButton!
    var delegate: RateViewDelegate?
    var stars: [UIButton?] = []
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let width: CGFloat = self.frame.width/5
        let height: CGFloat = self.frame.height
        self.stars = [starOne, starTwo, starthree, starFour, starFive]
        (0..<stars.count).forEach({ index in
            stars[index] = UIButton(frame: CGRect(x: CGFloat(index)*width, y: 0, width: width, height: height))
            self.addSubview(stars[index]!)
            stars[index]?.addTarget(self, action: #selector(self.starSelected(_:)), for: .touchUpInside)
            //stars[index]?.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
            stars[index]?.imageView?.image = #imageLiteral(resourceName: "ic_star")
            stars[index]?.imageView?.contentMode = .center
            stars[index]?.tintColor = .lightGray
            stars[index]?.tag = index
            
        })
    }
    
    //MARK: Targets
    @objc func starSelected(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        (0..<stars.count).forEach({ index in
            stars[index]?.tintColor = index <= sender.tag ? .orange : .lightGray
            
        })
        self.delegate?.rateSelected(index: sender.tag)
        
    }
    
}

