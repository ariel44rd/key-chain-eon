//
//  RoundedButton.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {

    // MARK: Attributes
    var bezierPath: UIBezierPath!
    @IBInspectable var color: UIColor!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.makeViewWith(features: [.color(.eonColor(.bgBlue)), .rounded])

    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code

    }
    */
}
