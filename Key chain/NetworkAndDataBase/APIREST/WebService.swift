//
//  File.swift
//  Key chain
//
//  Created by Ariel on 10/20/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

enum WebService {
    case getToken(String, String, String, String, String)
    case login(LoginRequest)
    case signup(SignupRequest)
    case forgotPassword(ForgotPasswordRequest)
    case posts(_ cveModule: String, _ userName: String)
    
    var baseURL: String {
        return App.Urls.baseURLREST
        
    }
    
    var requestURL: String {
        switch self {
        case .getToken, .login:
            return self.baseURL + "/Auth/Login"
            
        case .signup:
            return self.baseURL + "/Auth/RegisterNewUser"
            
        case .forgotPassword:
            return self.baseURL + "/Auth/PasswordRecovery"
            
        case .posts(let cveModule, let userName):
            return baseURL + "/Posts/ListarPosts/\(cveModule)/\(userName)"
            
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .login, .signup:
            return .post
            
        case .forgotPassword:
            return .put
            
        default:
            return .get
            
        }
    }
    
    var parameters: [String:Any]? {
        switch self {
        case .login(let loginRequest):
            return loginRequest.toJSON()
            
        case .signup(let signupRequest):
            return signupRequest.toJSON()
            
        case .forgotPassword(let forgotPasswordRequest):
            return forgotPasswordRequest.toJSON()
            
        default:
            return [:]
            
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .getToken:
            return [:]
            
        case .login:
            return ["Token": App.Token.login]
            
        case .signup:
            return ["Token": App.Token.sigupToken]
            
        case .forgotPassword:
            return ["Token": App.Token.forgotPasword]
            
        case .posts:
            if let userData: UserData = LocalStorage.getObjectWith(key: App.UserData.userDataKey) {
                return ["Token": userData.token]

            } else {
                return ["Token": ""]

            }
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .login, .signup, .forgotPassword:
            return JSONEncoding.default
            
        default:
            return URLEncoding.default
            
        }
    }
    
    var name: String {
        switch self {
        case .login:
            return "Login"
            
        case .signup:
            return "Signup"
            
        case .forgotPassword:
            return "Forgot Password"
            
        case .posts:
            return "Posts"
            
        default:
            return ""
            
        }
    }
    
    var showActivityViewWhileRequest: Bool {
        switch self {
        case .getToken:
            return false
            
        default:
            return true
            
        }
    }
}
