//
//  APIREST.swift
//  Key chain
//
//  Created by Ariel on 10/20/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

class APIManager {
    
    // MARK: Login
    func login(loginRequest: LoginRequest, completion: @escaping(LoginResponse?) -> Void) {
        self.endPoint(request: WebService.login(loginRequest), completion: { response in
            completion(response)
            
        })
    }
    
    // MARK: Signup
    func signup(signupRequest: SignupRequest, completion: @escaping(SignupResponse?) -> Void) {
        self.endPoint(request: WebService.signup(signupRequest), completion: { response in
            completion(response)
            
        })
    }
    
    // MARK: ForgotPassword
    func forgotPassword(forgotPasswordRequest: ForgotPasswordRequest, completion: @escaping(ForgotPasswordResponse?) -> Void) {
        self.endPoint(request: WebService.forgotPassword(forgotPasswordRequest), completion: { response in
            completion(response)
            
        })
    }
    
    // MARK: Posts
    func getPosts(cveModule: String, userName: String, completion: @escaping(PostsResponse?) -> Void) {
        self.endPoint(request: WebService.posts(cveModule, userName), completion: { response in
            completion(response)
            
        })
        
    }
    
    // MARK: EndPoint
    //(all API request pass through here)
    func endPoint<T: BaseResponse>(request: WebService, completion: @escaping(T?) -> Void) {
        debugPrint("URL: \(request.requestURL)")
        debugPrint("HEADERS: \(request.headers!)")
        debugPrint("PARAMETERS: \(request.parameters!)")
        if NetworkReachabilityManager()!.isReachable {
            if request.showActivityViewWhileRequest {
                ProgressActivityView.instance.progressViewWill(appear: true)
                
            }
            Alamofire.request(request.requestURL, method: request.method, parameters: request.parameters, encoding: request.encoding, headers: request.headers).validate().responseObject(completionHandler: { (response: DataResponse<T>) in
                ProgressActivityView.instance.progressViewWill(appear: false)
                switch response.result {
                case .success:
                    debugPrint("✔️ \(request.name)")
                    completion(response.value!)

                case .failure(let error):
                    debugPrint("❌ \(request.name) - ", error)
                    if error.localizedDescription.contains("The request timed out") {
                        debugPrint("TimeOut")
                        AlertManager.instance.showAlert(controller: AlertManager.instance.getTopViewController(), alertStyle: .alert, title: "Problemas de connexión", message: "No se ha establecido conexión con el servidor, intenta más tarde.", actions: nil)
                        
                    }
                    completion(nil)
                    
                }
            })
        } else {
            debugPrint("Network is not reachable!")
            let singletonAlert = AlertManager.instance
            singletonAlert.showAlert(controller: singletonAlert.getTopViewController(), alertStyle: .alert, title: "Problemas de connexión", message: "No hemos detectado una conexion a internet", actions: nil)
            completion(nil)
            
        }
    }
    
}

