//
//  BlogViewController.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

class BlogViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.open(URL(string: "https://itunes.apple.com/mx/app/compartamos-m%C3%B3vil/id1139377771?mt=8")!, options: [:], completionHandler: nil)

    }

    override func viewDidLayoutSubviews() {
        if self.setLayoutFlag {
            // Base features
            self.setLayoutFlag = false
            self.setUpBaseViewControllerWith(features: [
                .withStatusBar(nil),
                .withNavigationBar(nil),
                .title("Blog")
                
                ])
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
