//
//  DetailViewController.swift
//  Key chain
//
//  Created by Ariel on 10/22/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit
import YouTubePlayer

class DetailViewController: BaseViewController {

    // MARK: Attributes
    var post: Post!
    var userData: UserData? = LocalStorage.getObjectWith(key: App.UserData.userDataKey)
    
    // MARK: Outlets
    @IBOutlet weak var imgViewTop: UIImageView!
    @IBOutlet weak var tblView: UITableView!
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.separatorStyle = .none

    }
    
    override func viewDidLayoutSubviews() {
        if self.setLayoutFlag {
            // Base features
            self.setLayoutFlag = false
            self.setUpBaseViewControllerWith(features: [
                .withStatusBar(.eonColor(.bgBlue)),
                .withNavigationBar(.eonColor(.bgBlue)),
                .btnBack(#imageLiteral(resourceName: "ic_back")),
                
            ])
            self.btnBack.addTarget(self, action: #selector(self.backButton(_:)), for: .touchUpInside)
            self.btnBack.tintColor = .black
            self.lblTitle.minimumScaleFactor = 0.5
            self.lblTitle.adjustsFontSizeToFitWidth = true
            self.setPostDetail()
            self.statusBarView.alpha = 0
            self.navigationBarView.alpha = 0
            if let post = self.post as Post? {
                if post.contentTypeID == 4 {
                    ProgressActivityView.instance.progressViewWillOn(view: self.view, appear: true)

                }
            }
        }
    }
    
    // MARK: Targets
    @objc func backButton(_ sender: UIButton) {
        if self.navigationController?.popViewController(animated: true) == nil {
            self.dismiss(animated: true, completion: nil)
            
        }
    }
    
    func setPostDetail() {
        if let post = self.post as Post? {
            switch post.contentTypeID {
            case 2:
                self.imgViewTop.setImage(imgURL: URL(string: post.contentURL))

            default:
                self.imgViewTop.image = #imageLiteral(resourceName: "eon")
                self.imgViewTop.tintColor = .black
                self.imgViewTop.contentMode = .scaleAspectFit
                
            }
        }
    }

}

// MARK: UITableViewDelegate
extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.post.contentTypeID == 5 || self.post.contentTypeID == 4) ? 2 : 5
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.post.contentTypeID == 5 || self.post.contentTypeID == 4 {
            self.imgViewTop.isHidden = true
            let topedge = self.statusBarView.frame.height + navigationBarView.frame.height
            return indexPath.row == 0 ? topedge : self.workSpaceView.frame.height + 49
            
        }
        switch indexPath.row {
        case 0, 1, 4:
            return 0.9*self.imgViewTop.frame.height
            
        default:
            return 100
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.selectionStyle = .none
        switch indexPath.row {
        case 0:
            cell.makeViewWith(features: [.color(.clear)])
            cell.contentView.makeViewWith(features: [.color(.clear)])

        case 1:
            switch self.post.contentTypeID {
            case 4:
                let youTubePlayerView = YouTubePlayerView(frame: self.view.bounds)
                youTubePlayerView.loadVideoID(post.contentURL)
                youTubePlayerView.delegate = self
                cell.contentView.addSubview(youTubePlayerView)
                
            case 5:
                let webCell: WebViewTableViewCell = UIView.fromNib(nibName: App.Nibs.webViewTableViewCell)
                let request: URLRequest = URLRequest(url: URL(string: post.contentURL)!)
                webCell.webView.loadRequest(request)
                webCell.titleLabel.text = post.title
                webCell.webView.delegate = self
                return webCell
                
            default:
                if let post = self.post, let userData = self.userData {
                    let textCell: TextTableViewCell = UIView.fromNib(nibName: App.Nibs.textTableViewCell)
                    var text = post.postMessage.replacingOccurrences(of: "$(getNombre)$", with: userData.name)
                    text = text.replacingOccurrences(of: "%NOMBRE%", with: userData.name)
                    text = text.replacingOccurrences(of: "%USR_ADN%", with: userData.userName)
                    textCell.label.attributedText = try! NSAttributedString(htmlString: text, font: UIFont.systemFont(ofSize: 16), useDocumentFontSize: false)
                    textCell.selectionStyle = .none
                    return textCell
                    
                }
            }
        default:
            break
            
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        
    }
}

// MARK: UIScrollViewDelegate
extension DetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        if offset > 0 {
            self.imgViewTop.alpha = 1/(0.05*offset)
            self.statusBarView.alpha = self.imgViewTop.frame.height/(offset)
            self.navigationBarView.alpha = self.imgViewTop.frame.height/(offset)
            self.btnBack.tintColor = .white
            
        } else {
            self.imgViewTop.alpha = 1
            self.statusBarView.alpha = 0
            self.navigationBarView.alpha = 0
            self.btnBack.tintColor = .black
            
        }
    }
}

extension DetailViewController: UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        if let cell = self.tblView.cellForRow(at: IndexPath(row: 1, section: 0)) as? WebViewTableViewCell {
            ProgressActivityView.instance.progressViewWillOn(view: cell.webView.superview!, appear: true)

        }
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if let cell = self.tblView.cellForRow(at: IndexPath(row: 1, section: 0)) as? WebViewTableViewCell {
            ProgressActivityView.instance.progressViewWillOn(view: cell.webView.superview!, appear: false)
            
        }
    }
}

extension DetailViewController: YouTubePlayerDelegate {
    func playerReady(_ videoPlayer: YouTubePlayerView) {
        ProgressActivityView.instance.progressViewWillOn(view: videoPlayer, appear: false)
        
    }
}
