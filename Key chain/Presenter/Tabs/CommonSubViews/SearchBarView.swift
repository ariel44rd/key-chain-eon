//
//  SearchBarView.swift
//  Key chain
//
//  Created by Ariel on 10/22/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

class SearchBarView: UIView {

    @IBOutlet weak var searchBar: UISearchBar!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
