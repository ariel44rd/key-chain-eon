//
//  ProfileViewController.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {
    
    // MARK: Attributes
    let userData: UserData = LocalStorage.getObjectWith(key: App.UserData.userDataKey)!

    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        if self.setLayoutFlag {
            // Base features
            self.setLayoutFlag = false
            self.setUpBaseViewControllerWith(features: [
                .withStatusBar(nil),
                .withNavigationBar(nil),
                .withWorkSpace(.clear),
                .title("Perfil"),
                .tableView(self, .plain)
                
            ])
        }
    }

}

// MARK: UITableViewDelegate
extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell()
        let height: CGFloat = cell.frame.height
        let width: CGFloat = cell.frame.width
        let label: UILabel = UILabel(frame: CGRect(x: 20, y: 0, width: width, height: height/2))
        let bottomLabel: UILabel = UILabel(frame: CGRect(x: 20, y: height/2, width: width, height: height/2))
        cell.contentView.addSubview(label)
        cell.contentView.addSubview(bottomLabel)
        switch indexPath.row {
        case 0:
            label.text = "Nombre"
            bottomLabel.text = self.userData.name
            
        case 1:
            label.text = "Nombre de usuario"
            bottomLabel.text = self.userData.userName
            
        case 2:
            label.text = "E mail"
            bottomLabel.text = self.userData.email
            
        case 3:
            bottomLabel.text = "Cerrar sesión"
            
        default:
            break
            
        }
        label.textColor = .gray
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        if indexPath.row == 3 {
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancelar", style: .default, handler: nil)
            let okAction: UIAlertAction = UIAlertAction(title: "Aceptar", style: .destructive, handler: { _ in
                UserDefaults.standard.set(nil, forKey: App.UserData.userDataKey)
                self.navigationController?.popToRootViewController(animated: true)
                
            })
            AlertManager.instance.showAlert(controller: self, alertStyle: .alert, title: "Cerrar sesión", message: "¿Confirmas que deseas cerrar sesión?", actions: [cancelAction, okAction])
            
        }
    }
}
