//
//  PollViewController.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit
import AVKit

class PollViewController: BaseViewController {

    // MARK: Attributes
    var posts: [Post] = []
    var reuseIDCell: String = "collectionCell"
    var refreshCon: UIRefreshControl? = nil
    fileprivate let sectionInsets = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
    fileprivate let itemsPerRow: CGFloat = 2
    let numberOfItemsInSection: Int = 2
    
    // MARK: Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.refreshCon = {
            let refreshControl = UIRefreshControl()
            refreshControl.tintColor = .black
            return refreshControl
            
        }()
        self.collectionView.addSubview(self.refreshCon!)
        self.refreshCon?.addTarget(self, action: #selector(self.refreshControl(_:)), for: .valueChanged)
        self.getPosts()

    }
    
    override func viewDidLayoutSubviews() {
        if self.setLayoutFlag {
            // Base features
            self.setLayoutFlag = false
            self.setUpBaseViewControllerWith(features: [
                .withStatusBar(nil),
                .withNavigationBar(nil),
                .title("Encuesta"),
                
            ])
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.register(CollectionCell.self, forCellWithReuseIdentifier: self.reuseIDCell)

        }
    }

    // MARK: APIMethods
    func getPosts() {
        if let userData: UserData = LocalStorage.getObjectWith(key: App.UserData.userDataKey) {
            let cveModule: String = "MODULO2"
            HomeInteractor.getPosts(cveModule: cveModule, userName: userData.userName, completion: { response in
                self.posts = response
                self.collectionView.reloadData()
                
            })
        }
    }
    
    // MARK: Targets
    @objc func refreshControl(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PollViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return (posts.count % numberOfItemsInSection == 0) ? (posts.count/numberOfItemsInSection) : (1 + posts.count/numberOfItemsInSection)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (posts.count - 3*section) > 3 ? 3 : posts.count - 3*section
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: self.reuseIDCell, for: indexPath) as! CollectionCell
        let post = posts[indexPath.item]
        if post.contentTypeID == 2 {
            cell.imgBrand.setImage(imgURL: URL(string: post.contentURL))
            
        } else {
            cell.imgBrand.image = UIImage(named: "ic_play")
            cell.imgBrand.tintColor = .black
            
        }
        cell.lblNameBrand.text = post.title
        cell.contentView.makeViewWith(features: [
            .color(.white),
            .shadow
            
        ])
        cell.setCollectionCell()
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        debugPrint("indexPath: [section: \(indexPath.section), row: \(indexPath.row)]")
        let post: Post = self.posts[indexPath.item]
        switch post.contentTypeID {
        case 3:
            let url: URL = URL(string: post.contentURL)!
            let player = AVPlayer(url: url)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
                
            }
        default:
            let storyboard = UIStoryboard(name: "Detail", bundle: nil)
            let detailViewController: DetailViewController = storyboard.instantiateInitialViewController() as! DetailViewController
            detailViewController.post = self.posts[indexPath.item]
            self.present(detailViewController, animated: true, completion: nil)
            
        }
    }
    
    // CollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / (1.1*self.itemsPerRow)
        return CGSize(width: widthPerItem, height: 1.5*widthPerItem)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
}
