//
//  CollectionViewCell.swift
//  Key chain
//
//  Created by Ariel on 10/22/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

class CollectionCell: UICollectionViewCell {
    
    // MARK: Instances
    var imgBrand: UIImageView!
    var lblNameBrand: UILabel!
    
    // MARK: Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.imgBrand = UIImageView()
        self.lblNameBrand = UILabel()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        
    }
    
    func setCollectionCell() {
        self.contentView.addSubview(self.imgBrand)
        self.contentView.addSubview(self.lblNameBrand)
        self.imgBrand.frame = CGRect(
            x: 5,
            y: 5,
            width: self.contentView.frame.width - 10,
            height: 0.8*self.contentView.frame.height - 5
        )
        self.imgBrand.contentMode = .scaleAspectFit
        self.lblNameBrand.frame = CGRect(
            x: 0,
            y: 0.8*self.contentView.frame.height,
            width: self.contentView.frame.width,
            height: 0.2*self.contentView.frame.height
        )
        self.lblNameBrand.textAlignment = .center
        self.lblNameBrand.adjustsFontSizeToFitWidth = true
        self.lblNameBrand.minimumScaleFactor = 0.5
        self.lblNameBrand.numberOfLines = 0
        
    }
}

