//
//  ImageTableViewCell.swift
//  Key chain
//
//  Created by Ariel on 10/22/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {

    // MARK: Outlets
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    // MARK: Overrides
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
