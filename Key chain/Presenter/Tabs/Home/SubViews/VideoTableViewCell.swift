//
//  VideoTableViewCell.swift
//  Key chain
//
//  Created by Ariel on 10/22/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit
import AVKit
protocol VideoPlayingCellProtocol : NSObjectProtocol {
    func playVideoForCell(with indexPath : IndexPath)
    func PreparedVideoForShow(playerViewController:AVPlayerViewController)
}
class VideoTableViewCell: UITableViewCell {
    @IBOutlet weak var fechaLabel: UILabel!
    @IBOutlet weak var iconoImageView: UIImageView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var thumnailVideo: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    
    
    
    var playerController : AVPlayerViewController? = nil
    var passedURL : URL! = nil
    var indexPath : IndexPath! = nil
    var delegate : VideoPlayingCellProtocol? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        playerController = AVPlayerViewController()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VideoTableViewCell.imageTapped(tapGestureRecognizer:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.numberOfTouchesRequired = 1
        self.selectionStyle = .none
        
        thumnailVideo.isUserInteractionEnabled = true
        thumnailVideo.addGestureRecognizer(tapGestureRecognizer)
    }
    
    
    
    
    func configCell(with url : URL,shouldPlay : Bool) {
        //something like this
        self.passedURL = url
        if shouldPlay == true {
            let player = AVPlayer(url: url)
            if self.playerController == nil {
                playerController = AVPlayerViewController()
            }
            playerController?.player = player
            playerController?.showsPlaybackControls = true
            playerController?.player?.play()
        }
        else {
            if self.playerController != nil {
                self.playerController?.player?.pause()
                self.playerController?.player = nil
            }
            var icono:UIImage
            icono = #imageLiteral(resourceName: "ic_play")
            thumnailVideo?.image = icono
            
            //show video thumbnail with play button on it.
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let player = AVPlayer(url: passedURL)
        if self.playerController == nil {
            playerController = AVPlayerViewController()
        }
        playerController?.player = player
        playerController?.showsPlaybackControls = true
        playerController?.player?.play()
        self.delegate?.PreparedVideoForShow(playerViewController: playerController!)
        //addSubview((playerController?.view)!)
            
    }
    
    override func prepareForReuse() {
        //this way once user scrolls player will pause
        self.playerController?.player?.pause()
        self.playerController?.player = nil
    }
    
    
}

extension UILabel {
    var optimalHeight : CGFloat {
        get
        {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.lineBreakMode = NSLineBreakMode.byWordWrapping
            label.font = self.font
            label.text = self.text
            label.sizeToFit()
            return label.frame.height
        }
        
    }
}
