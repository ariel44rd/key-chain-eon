//
//  HomeviewControllerViewController.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit
import AVKit

class HomeViewController: BaseViewController {

    // MARK: Attributes
    var posts: [Post] = []
    var filteredPost: [Post] = []
    let searchBarView: SearchBarView = UIView.fromNib(nibName: App.Nibs.searchBar)
    var currentlyPlayingIndexPath : IndexPath? = nil
    var filtered: Bool = false
    var tapGesture: UITapGestureRecognizer!
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.searchBarWill(appear: false, animated: false)
        self.getPosts()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
        
    }
    
    override func viewDidLayoutSubviews() {
        if self.setLayoutFlag {
            // Base features
            self.setLayoutFlag = false
            self.setUpBaseViewControllerWith(features: [
                .withStatusBar(nil),
                .withNavigationBar(nil),
                .title("Inicio"),
                .withWorkSpace(.white),
                .tableView(self, .grouped),
                .refreshControl(.eonColor(.bgBlue), nil),
                .btnRight(#imageLiteral(resourceName: "ic_back"))
                
            ])
            self.refreshControl?.addTarget(self, action: #selector(self.refreshControl(_:)), for: .valueChanged)
            self.view.addSubview(self.searchBarView)
            self.searchBarView.searchBar.delegate = self
            let yPosition: CGFloat = self.statusBarView.frame.height + self.navigationBarView.frame.height
            self.searchBarView.frame = CGRect(x: 0, y: yPosition, width: workSpaceView.frame.width, height: 50)
            self.btnRight.addTarget(self, action: #selector(self.searchBarAppear(_:)), for: .touchUpInside)
            self.btnRight.setImage(nil, for: .normal)
            self.btnRight.setTitle("Buscar", for: .normal)
            self.btnRight.setTitleColor(.white, for: .normal)
            
        }
    }
    
    // MARK: APIMethods
    func getPosts() {
        if let userData: UserData = LocalStorage.getObjectWith(key: App.UserData.userDataKey) {
            let cveModule: String = "MODULO1"
            HomeInteractor.getPosts(cveModule: cveModule, userName: userData.userName, completion: { response in
                self.posts = response
                self.tableView.reloadData()
                
            })
        }
    }
    
    // MARK: Targets
    @objc func refreshControl(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.getPosts()
        
    }
    
    @objc func dismissKeyboard() {
        self.searchBarWill(appear: false, animated: true)
        self.view.endEditing(true)
        
    }
    
    @objc func searchBarAppear(_ sender: UIButton) {
        self.searchBarWill(appear: true, animated: true)
        
    }
    
    // MARK: Methods
    func searchBarWill(appear: Bool, animated: Bool) {
        if appear {
            self.view.addGestureRecognizer(self.tapGesture)

        } else {
            self.view.removeGestureRecognizer(self.tapGesture)
            
        }
        UIView.animate(withDuration: animated ? 0.5 : 0, animations: {
            self.searchBarView.alpha = appear ? 1 : 0
            
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: UITableViewDelegate
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filtered ? self.filteredPost.count : self.posts.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let post: Post = self.filtered ? self.filteredPost[indexPath.row] : self.posts[indexPath.row]
        switch post.contentTypeID {
        case 1:
            return 80
            
        case 3:
            return 200
            
        default:
            return 200
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post: Post = self.filtered ? self.filteredPost[indexPath.row] : self.posts[indexPath.row]
        let postCell: PostTableViewCell = UIView.fromNib(nibName: App.Nibs.postTableViewCell)
        switch post.contentTypeID {
        case 0:
            break
            
        case 1:
            let textCell: TextTableViewCell = UIView.fromNib(nibName: App.Nibs.textTableViewCell)
            textCell.label.text = post.title
            textCell.titleLabel.isHidden = true
            textCell.label.textColor = .white
            textCell.label.superview?.makeViewWith(features: [.color(.black)])
            return textCell
            
        case 2: //image
            let imageCell: ImageTableViewCell = UIView.fromNib(nibName: App.Nibs.imageTableViewCell)
            imageCell.imgView.setImage(imgURL: URL(string: post.contentURL))
            imageCell.label.text = post.title
            return imageCell
            
        case 3: // video
            let videoCell: VideoTableViewCell = UIView.fromNib(nibName: App.Nibs.videCell)
            videoCell.delegate = self
            videoCell.selectionStyle = .none
            let withContent: Bool = (post.postTypeID != 1)
            videoCell.indexPath = indexPath
            videoCell.messageLabel.text = post.title
            videoCell.messageLabel.lineBreakMode = .byWordWrapping
            videoCell.messageLabel.numberOfLines = 0
            videoCell.messageLabel.font = UIFont.systemFont(ofSize: 16)
//            if ((videoCell.messageLabel.text?.contains("<span"))! || (videoCell.messageLabel.text?.contains("<p"))! || (videoCell.messageLabel.text?.contains("<b"))!) {
//                videoCell.messageLabel.attributedText = try! NSAttributedString(htmlString: post.postMessage!, font: UIFont.systemFont(ofSize: 16), useDocumentFontSize: withContent)
//
//            }
            if !(post.contentURL?.isEmpty)! {
                videoCell.configCell(with: URL(string: post.contentURL!)!, shouldPlay: self.currentlyPlayingIndexPath == indexPath)
            }
            return videoCell
            
        case 4: // Youtube
            let textCell: TextTableViewCell = UIView.fromNib(nibName: App.Nibs.textTableViewCell)
            textCell.titleLabel.text = post.title
            textCell.label.attributedText = try! NSAttributedString(htmlString: post.postMessage!, font: UIFont.systemFont(ofSize: 15), useDocumentFontSize: false)
            return textCell
            
        case 5: //web
            let textCell: TextTableViewCell = UIView.fromNib(nibName: App.Nibs.textTableViewCell)
            textCell.titleLabel.text = post.title
            textCell.label.attributedText = try! NSAttributedString(htmlString: post.postMessage!, font: UIFont.systemFont(ofSize: 15), useDocumentFontSize: false)
            return textCell
            
        default:
            break
            
        }
        return postCell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)!.setSelected(false, animated: true)
        let post: Post = self.filtered ? self.filteredPost[indexPath.row] : self.posts[indexPath.row]
        switch post.contentTypeID {
        case 3:
            break
            
        default:
            let storyboard = UIStoryboard(name: "Detail", bundle: nil)
            let detailViewController: DetailViewController = storyboard.instantiateInitialViewController() as! DetailViewController
            detailViewController.post = post
            self.present(detailViewController, animated: true, completion: nil)
            
        }
    }
    
}

// MARK: VideoPlayerCellProtocol
extension HomeViewController : VideoPlayingCellProtocol {
    func PreparedVideoForShow(playerViewController: AVPlayerViewController) {
        self.present(playerViewController, animated: true) {
            //playerViewController.player?.play()
        }
    }
    
    func playVideoForCell(with indexPath: IndexPath) {
        self.currentlyPlayingIndexPath = indexPath
        //reload tableView
        self.tableView.reloadRows(at: self.tableView.indexPathsForVisibleRows!, with: .none)
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filtered = searchText != ""
        debugPrint(searchText)
        self.filteredPost = self.posts.filter({
            $0.title.contains(searchText)
            
        })
        self.tableView.reloadData()
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        debugPrint("cancelSearch")
        self.filtered = false
        self.dismissKeyboard()
        self.searchBarWill(appear: false, animated: true)
        self.tableView.reloadData()
        
    }
}
