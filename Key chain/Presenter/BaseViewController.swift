//
//  BaseViewController.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

enum ViewControllerFeature {
    case withStatusBar(UIColor?),
    withNavigationBar(UIColor?),
    withWorkSpace(UIColor),
    btnBack(UIImage),
    btnRight(UIImage),
    tableView(UIViewController, UITableView.Style),
    refreshControl(UIColor, UITableView?),
    bgColor(UIColor),
    title(String),
    navigationBarImage(UIImage)
    
}

class BaseViewController: UIViewController {
    
    // MARK: Instances
    var statusBarView: UIView!
    var navigationBarView: UIView!
    var workSpaceView: UIView!
    var heightForStatusBar: CGFloat!
    var heightForNavigationBar: CGFloat!
    var heightForWorkSpace: CGFloat!
    var btnBack: UIButton!
    var btnRight: UIButton!
    var tableView: UITableView!
    var setLayoutFlag: Bool!
    var lblTitle: UILabel!
    var navigationBarImage: UIImageView!
    var heightForHeader: CGFloat!
    var heightForCell: CGFloat!
    var overrideBtnBack: Bool = false
    var refreshControl:UIRefreshControl? = nil
    var heightForTabBar: CGFloat = 49
    let workSpaceEdge: CGFloat = 5
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //SetLayout
        self.heightForStatusBar = self.isIphoneX() ? 40 : 20
        self.heightForNavigationBar = 44
        let yPositionForWorkSpace = self.heightForStatusBar + self.heightForNavigationBar
        self.heightForWorkSpace = self.view.frame.height - yPositionForWorkSpace
        self.statusBarView = UIView(frame: CGRect(
            x: 0,
            y: 0,
            width: self.view.frame.width,
            height: self.heightForStatusBar)
            
        )
        self.navigationBarView = UIView(frame: CGRect(
            x: 0,
            y: self.statusBarView.frame.height,
            width: self.view.frame.width,
            height: self.heightForNavigationBar)
            
        )
        self.workSpaceView = UIView(frame: CGRect(
            x: 0,
            y: yPositionForWorkSpace,
            width: self.view.frame.width,
            height: self.heightForWorkSpace - (heightForTabBar))
            
        )
        self.btnBack = UIButton(frame: CGRect(x: 0, y: self.statusBarView.frame.height, width: 50, height: self.navigationBarView.frame.height))
        self.btnRight = UIButton(frame: CGRect(x: self.navigationBarView.frame.width - 100, y: 0, width: 100, height: self.navigationBarView.frame.height))
        self.setLayoutFlag = true
        self.lblTitle = UILabel(frame: CGRect(x: self.navigationBarView.center.x - 100, y: 0, width: 200, height: self.navigationBarView.frame.height))
        self.navigationBarImage = UIImageView(frame: CGRect(x: self.navigationBarView.center.x - 100, y: 0, width: 200, height: self.navigationBarView.frame.height))
        self.heightForHeader = 50
        self.heightForCell = 50
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    // MARK: Methods
    //Check if statusBarHeight is greater than standard iphone statusBar
    func isIphoneX() -> Bool {
        let device = UIDevice()
        debugPrint("device name: [\(device.name)]")
        debugPrint("height:      [\(UIScreen.main.nativeBounds.height)]")
        return device.userInterfaceIdiom == .phone && device.name.contains("iPhone X")
        
    }
    
    func setUpBaseViewControllerWith(features: [ViewControllerFeature]) {
        features.forEach({
            switch $0 {
            case .withStatusBar(let color):
                self.view.addSubview(self.statusBarView)
                self.statusBarView.makeViewWith(features: [.color(color ?? .eonColor(.bgBlue))])
                
            case .withNavigationBar(let color):
                self.view.addSubview(self.navigationBarView)
                self.navigationBarView.makeViewWith(features: [.color(color ?? .eonColor(.bgBlue))])
                
            case .withWorkSpace(let color):
                self.view.addSubview(self.workSpaceView)
                self.workSpaceView.makeViewWith(features: [.color(color)])
                
            case .btnBack(let image):
                self.view.addSubview(self.btnBack)
                self.btnBack.setImage(image, for: .normal)
                
            case .btnRight(let image):
                self.navigationBarView.addSubview(self.btnRight)
                self.btnRight.setImage(image, for: .normal)
                self.btnRight.tintColor = .white
                
            case .tableView(let controller, let style):
                self.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.workSpaceView.frame.height), style: style)
                self.tableView.delegate = controller as? UITableViewDelegate
                self.tableView.dataSource = controller as? UITableViewDataSource
                self.workSpaceView.addSubview(self.tableView)
                //                self.sadCloudView = ®(frame: self.workSpaceView.frame)
                //                self.tableView.addSubview(sadCloudView.shape(.sadCloud, UIColor.black.withAlphaComponent(0.5)))
                //                sadCloudView.isHidden = true
                
            case .refreshControl(let color , let tableView):
                self.refreshControl = {
                    let refreshControl = UIRefreshControl()
                    refreshControl.tintColor = color
                    return refreshControl
                    
                }()
                if let tableView = tableView as UITableView? {
                    tableView.addSubview(self.refreshControl!)

                } else {
                    self.tableView.addSubview(self.refreshControl!)
                    
                }
            case .bgColor(let color):
                self.view.makeViewWith(features: [.color(color)])
                
            case .title(let title):
                self.lblTitle.text = title
                self.lblTitle.textAlignment = .center
                self.lblTitle.textColor = .white
                self.lblTitle.font = UIFont(name: "Helvetica", size: 25)
                self.navigationBarView.addSubview(self.lblTitle)
                
            case .navigationBarImage(let image):
                self.navigationBarImage.image = image
                self.navigationBarView.addSubview(self.navigationBarImage)
                self.navigationBarImage.contentMode = .scaleAspectFit
                
            }
        })
    }
    
    @objc func btnRightTarget(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2, animations: {
            sender.transform = CGAffineTransform(rotationAngle: 1.08*CGFloat(Double.pi))
            
        }, completion: { _ in
            //"{\"NoQuestion\":1,\"Command\":\"#launch_question\"}" |> TriviaInteractor.instance.socket.write
            let storyboard = UIStoryboard(name: "Settings", bundle: nil)
            let viewController = storyboard.instantiateInitialViewController()
            self.present(viewController!, animated: true, completion: {
                self.btnRight.removeFromSuperview()
                self.btnRight = UIButton(frame: CGRect(x: self.navigationBarView.frame.width - 50, y: 0, width: 50, height: self.navigationBarView.frame.height))
                self.setUpBaseViewControllerWith(features: [.btnRight(#imageLiteral(resourceName: "ic_settings"))])
                
            })
        })
    }
    
}

