//
//  ViewController.swift
//  Key chain
//
//  Created by Ariel on 10/20/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard)))
        self.userNameTextField.delegate = self
        self.passwordTextField.delegate = self
        
    }
    
    // MARK: Actions
    @IBAction func loginButton(_ sender: Any) {
        LoginInteractor.login(userName: self.userNameTextField.text!, password: self.passwordTextField.text!, completion: { response in
            self.performSegue(withIdentifier: "tabBarSegue", sender: nil)

        })
    }
    
    @IBAction func signupButton(_ sender: Any) {
        self.performSegue(withIdentifier: "signupSegue", sender: nil)
        
    }
    
    @IBAction func forgotPasswordButton(_ sender: Any) {
        self.performSegue(withIdentifier: "forgotPasswordSegue", sender: nil)
        
    }
    
    // MARK: Targets
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
        
    }
}

// MARK: UITextfieldDelegate
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.userNameTextField:
            self.passwordTextField.becomeFirstResponder()
            
        case self.passwordTextField:
            self.dismissKeyboard()
            self.loginButton("")
            
        default:
            break
        }
        return true
    }
}

