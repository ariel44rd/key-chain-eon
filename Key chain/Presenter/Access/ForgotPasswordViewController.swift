//
//  ForgotPasswordViewController.swift
//  Key chain
//
//  Created by Ariel on 10/22/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseViewController {
    
    // MARK: Outlets
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var eMailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard)))

    }
    
    override func viewDidLayoutSubviews() {
        if self.setLayoutFlag {
            // Base features
            self.setLayoutFlag = false
            self.setUpBaseViewControllerWith(features: [
                .withStatusBar(nil),
                .withNavigationBar(nil),
                .title("Recuperar contraseña"),
                .btnBack(#imageLiteral(resourceName: "ic_back")),
                
            ])
            self.btnBack.addTarget(self, action: #selector(self.backButton(_:)), for: .touchUpInside)
            self.btnBack.tintColor = .white
            self.lblTitle.minimumScaleFactor = 0.5
            self.lblTitle.adjustsFontSizeToFitWidth = true
            
        }
    }
    
    // MARK: Actions
    @IBAction func recoveryPasswordButton(_ sender: Any) {
        let forgotPasswordJsonString: String = """
            {
                "ObjetoPrincipal": {
                "email": "\(self.eMailTextField.text!)",
                "userName": "\(self.userNameTextField.text!)",
                "NewPassword": "\(self.passwordTextField.text!)"
                },
                "IdUsuario": 1,
                "VersionApp": "\(App.MetaData.appVersion)",
                "IdDispositivo": "\(App.MetaData.deviceID)",
                "Token": "\(App.Token.forgotPasword)"
            }
        """
        if let forgotPasswordRequest: ForgotPasswordRequest = JSONManager.getObject(jsonString: forgotPasswordJsonString) {
            if self.passwordTextField.text! == self.confirmPasswordTextField.text! {
                ForgotPasswordInteractor.forgotPassword(forgotPasswordRequest: forgotPasswordRequest, completion: { response in
                    debugPrint(response)
                    if response {
                        let okAction = UIAlertAction(title: "", style: .default, handler: { _ in
                            if self.navigationController?.popViewController(animated: true) == nil {
                                self.dismiss(animated: true, completion: nil)
                                
                            }
                        })
                        AlertManager.instance.showAlert(controller: self, alertStyle: .alert, title: "", message: "Se te ha enviado un e mail a \(self.eMailTextField.text!)", actions: [okAction])
                        
                    } else {
                        AlertManager.instance.showAlert(controller: self, alertStyle: .alert, title: "", message: "No se pudo realizar la petición, por favor vuelve a intentar", actions: nil)
                        
                    }
                })
            } else {
                AlertManager.instance.showAlert(controller: self, alertStyle: .alert, title: "Error", message: "Las Contraseñas deben ser iguales", actions: nil)
                
            }
        }
    }
    
    // MARK: Targets
    @objc func backButton(_ sender: UIButton) {
        if self.navigationController?.popViewController(animated: true) == nil {
            self.dismiss(animated: true, completion: nil)
            
        }
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
