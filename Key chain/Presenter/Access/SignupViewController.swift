//
//  SignupViewController.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

enum PickerType {
    case birthDay, gender
    
}

class SignupViewController: BaseViewController {
    
    // MARK: Attributes
    var pickerType: PickerType!
    var genders: [String] = ["Femenino", "Masculino"]
    
    // MARK: Outlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextfield: UITextField!
    @IBOutlet weak var mothersLastNameTextField: UITextField!
    @IBOutlet weak var userNameTextfield: UITextField!
    @IBOutlet weak var birthDayTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextfield: UITextField!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var genderPicker: UIPickerView!
    
    // MARK: OVerrides
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard)))
        self.blurView.alpha = 0
        self.datePicker.alpha = 0
        self.genderPicker.alpha = 0
        self.genderPicker.delegate = self
        self.genderPicker.dataSource = self
        
        self.nameTextField.delegate = self
        self.lastNameTextfield.delegate = self
        self.mothersLastNameTextField.delegate = self
        self.userNameTextfield.delegate = self
        self.birthDayTextField.delegate = self
        self.genderTextField.delegate = self
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        self.confirmPasswordTextfield.delegate = self

    }
    
    override func viewDidLayoutSubviews() {
        if self.setLayoutFlag {
            // Base features
            self.setLayoutFlag = false
            self.setUpBaseViewControllerWith(features: [
                .withStatusBar(nil),
                .withNavigationBar(nil),
                .title("Registro"),
                .btnBack(#imageLiteral(resourceName: "ic_back")),
                
            ])
            self.btnBack.addTarget(self, action: #selector(self.backButton(_:)), for: .touchUpInside)
            self.btnBack.tintColor = .white
            
        }
    }
    
    // MARK: Actions
    @IBAction func signupButton(_ sender: Any) {
        self.dismissKeyboard()
        let signupData: SignupData = SignupData(
            userNameTextfield.text!,
            emailTextField.text!,
            nameTextField.text!,
            lastNameTextfield.text!,
            mothersLastNameTextField.text!,
            birthDay: birthDayTextField.text!,
            gender: genderTextField.text! == "Masculino" ? 1 : 2,
            password: passwordTextField.text!
        )
        if passwordTextField.text == confirmPasswordTextfield.text {
            SignupInteractor.signup(signupData: signupData, completion: { response in
                if response {
                    let okAction = UIAlertAction(title: "Aceptar", style: .default, handler: { _ in
                        if self.navigationController?.popViewController(animated: true) == nil {
                            self.dismiss(animated: true, completion: nil)
                            
                        }
                    })
                    AlertManager.instance.showAlert(controller: self, alertStyle: .alert, title: "Registro exitoso", message: "Te hemos enviado un email a \(self.emailTextField.text!), el cual deberás confirmar para poder acceder.", actions: [okAction])
                    
                } else {
                    AlertManager.instance.showAlert(controller: self, alertStyle: .alert, title: "Error", message: "No se ha podido hacer el registro", actions: nil)
                    
                }
            })
        } else {
            AlertManager.instance.showAlert(controller: self, alertStyle: .alert, title: "Error", message: "Las contraseñas no coinciden", actions: nil)
            
        }
    }
    
    @IBAction func birthDayButton(_ sender: Any) {
        self.dismissKeyboard()
        self.genderPicker.alpha = 0
        self.pickerType = .birthDay
        UIView.animate(withDuration: 0.5, animations: {
            self.blurView.alpha = 1
            self.datePicker.alpha = 1
            
        })
    }
    
    @IBAction func genderButton(_ sender: Any) {
        self.genderPicker.reloadComponent(0)
        self.dismissKeyboard()
        self.datePicker.alpha = 0
        self.pickerType = .gender
        UIView.animate(withDuration: 0.5, animations: {
            self.blurView.alpha = 1
            self.genderPicker.alpha = 1
            
        })
    }
    
    @IBAction func cancelPicker(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.blurView.alpha = 0
            self.datePicker.alpha = 0
            self.genderPicker.alpha = 0
            
        })
    }
    
    @IBAction func donePicker(_ sender: Any) {
        self.cancelPicker("")
        if self.pickerType == .birthDay {
            self.birthDayTextField.setDate(date: self.datePicker.date, dateFormat: .ddMMyyyy)
            self.genderButton("")
            
        } else {
            let index = self.genderPicker.selectedRow(inComponent: 0)
            self.genderTextField.text = index == 0 ? "Femenino" : "Masculino"
            self.emailTextField.becomeFirstResponder()
            
        }
    }
    
    // MARK: Targets
    @objc func backButton(_ sender: UIButton) {
        if self.navigationController?.popViewController(animated: true) == nil {
            self.dismiss(animated: true, completion: nil)
            
        }
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
        
    }
    
    // MARK: Methods
    func pickerViewWill(appear: Bool) {
        self.dismissKeyboard()
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: UITextFieldDelegate
extension SignupViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.nameTextField:
            self.lastNameTextfield.becomeFirstResponder()
            
        case self.lastNameTextfield:
            self.mothersLastNameTextField.becomeFirstResponder()
            
        case self.mothersLastNameTextField:
            self.userNameTextfield.becomeFirstResponder()
            
        case self.userNameTextfield:
            self.birthDayButton("")
            
        case self.birthDayTextField:
            self.genderButton("")
            
        case self.genderTextField:
            self.emailTextField.becomeFirstResponder()
            
        case self.emailTextField:
            self.passwordTextField.becomeFirstResponder()
            
        case self.passwordTextField:
            self.confirmPasswordTextfield.becomeFirstResponder()
            
        case self.confirmPasswordTextfield:
            self.signupButton("")
            
        default:
            break
            
        }
        return true
    }
}

extension SignupViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genders[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
        
    }
    
}
