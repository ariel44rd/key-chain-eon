//
//  SplashViewController.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.forward)))

    }
    
    // MARK: Targets
    @objc func forward() {
        if let _: UserData = LocalStorage.getObjectWith(key: App.UserData.userDataKey) {
            self.performSegue(withIdentifier: "isLogedSegue", sender: nil)
            
        } else {
            self.performSegue(withIdentifier: "loginSegue", sender: nil)
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
