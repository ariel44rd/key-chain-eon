//
//  HomeInteractor.swift
//  Key chain
//
//  Created by Ariel on 10/22/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

class HomeInteractor {
    class func getPosts(cveModule: String, userName: String, completion: @escaping([Post]) -> Void) {
        APIManager().getPosts(cveModule: cveModule, userName: userName, completion: { response in
            if let response = response as PostsResponse?, let posts = response.mainObject as [Post]? {
                completion(posts)
                
            }
        })
    }
}
