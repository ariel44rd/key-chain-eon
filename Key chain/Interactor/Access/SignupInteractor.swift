//
//  SignupInteractor.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

class SignupInteractor {
    
    // MARK: Methods
    class func signup(signupData: SignupData, completion: @escaping(Bool) -> Void) {
        let signupRequest: SignupRequest = SignupRequest(userID: 1, appVersion: App.MetaData.appVersion, deviceID: App.MetaData.deviceID, token: "", signupData: signupData)
        APIManager().signup(signupRequest: signupRequest, completion: { response in
            debugPrint(response?.toJSON())
            if let response = response as SignupResponse? {
                completion(response.mainObject)
                
            }
        })
    }
    
}
