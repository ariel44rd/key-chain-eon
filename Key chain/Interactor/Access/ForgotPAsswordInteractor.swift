//
//  ForgotPAsswordInteractor.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

class ForgotPasswordInteractor {
    
    class func forgotPassword(forgotPasswordRequest: ForgotPasswordRequest, completion: @escaping(Bool) -> Void) {
        APIManager().forgotPassword(forgotPasswordRequest: forgotPasswordRequest, completion: { response in
            debugPrint(response)
            if let response = response, let mainObject = response.mainObject {
                completion(mainObject)

            }
        })
    }
    
}
