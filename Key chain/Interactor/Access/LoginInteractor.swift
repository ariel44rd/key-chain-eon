//
//  LoginInteractor.swift
//  Key chain
//
//  Created by Ariel on 10/20/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

class LoginInteractor {
    
    // MARK: Attributes
    
    // MARK: Methods
    class func login(userName: String, password: String, completion: @escaping(UserData) -> Void) {
        let stringDummieObject = """
            {
              "ObjetoPrincipal": {
                "userName": "\(userName)",
                "password": "\(password)"
              },
              "IdUsuario": 0,
              "VersionApp": "\(App.MetaData.appVersion)",
              "IdDispositivo": "\(App.MetaData.deviceID)",
              "Token": ""
            }
        """
        if let loginRequest: LoginRequest = JSONManager.getObject(jsonString: stringDummieObject) {
            APIManager().login(loginRequest: loginRequest, completion: { response in
                debugPrint(response?.toJSONString(prettyPrint: true))
                if response != nil && response!.userData != nil && response!.userData!.validate() {
                    LocalStorage.saveObject(object: response!.userData!, key: App.UserData.userDataKey)
                    completion(response!.userData!)
                    
                } else {
                    AlertManager.instance.showAlert(controller: nil, alertStyle: .alert, title: "Error", message: "Datos incorrectos, por favor intenta nuevamente", actions: nil)
                    
                }
            })
        }
    }
    
}
