//
//  UIColorExtension.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

enum EONColorEnum {
    case bgBlue, bgOrange
    
}

extension UIColor {
    
    static func eonColor(_ color: EONColorEnum) -> UIColor {
        switch color {
        case .bgBlue:
            return UIColor(red: 50/255.0, green: 62.0/255.0, blue: 72.0/255.0, alpha: 1.0)
            
        case .bgOrange:
            return UIColor(red: 50/255.0, green: 62.0/255.0, blue: 72.0/255.0, alpha: 1.0)

        }
    }
    
}
