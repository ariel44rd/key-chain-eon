//
//  UIViewExtension.swift
//  Key chain
//
//  Created by Ariel on 10/20/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

enum ViewFeatures {
    case rounded, shadow, color(UIColor), bordered(UIColor, CGFloat), image(UIImage), roundedView(RoundedType, UIColor), topRounded, fullRounded, layerAnimation(_ duration: Double, _ animated: Bool)
}

enum RoundedType {
    case full, onlyLayer
}

extension UIView {
    
    class func fromNib<T: Any>(nibName: String) -> T {
        return UINib(nibName: nibName, bundle: nil).instantiate(withOwner: nil, options: nil).first! as! T
        
    }
    
    func makeViewWith(features: [ViewFeatures]?) {
        if let features = features as [ViewFeatures]? {
            features.forEach({
                switch $0 {
                case .rounded:
                    self.layer.cornerRadius = self.frame.height/2
                    
                case .shadow:
                    self.setShadow()
                    
                case .color(let color):
                    self.backgroundColor = color
                    
                case .bordered(let color, let borderWidth):
                    self.setCornerRadius(color: color, borderWidth: borderWidth)
                    
                case .image(let image):
                    (self as! UIImageView).image = image
                    
                case .roundedView(let roundedType, let color):
                    self.setRounded(roundedType: roundedType, color: color)
                    
                case .topRounded:
                    self.setTopRounded()
                    
                case .fullRounded:
                    self.setFullRounded()
                    
                case .layerAnimation(let duration, let animated):
                    self.animationtrigger(duration: animated ? duration : 0)
                    
                }
            })
        }
    }
    
    func setTopRounded() {
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners:[.topRight, .topLeft],
                                cornerRadii: CGSize(width: 30, height:  30))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    func setFullRounded() {
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners:[.topRight, .topLeft, .bottomLeft, .bottomRight],
                                cornerRadii: CGSize(width: 10, height:  10))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    func setPairButtons(btnSelected: UIButton, btnDeselected: UIButton, constraints: [NSLayoutConstraint], originalHeight: CGFloat) {
        btnSelected.isSelected = !btnSelected.isSelected
        if btnSelected.isSelected {
            constraints[0].constant = originalHeight + 40
            constraints[1].constant = originalHeight
            btnDeselected.isSelected = false
            btnSelected.imageView?.alpha = 1
            btnDeselected.imageView?.alpha = 0.3
            
        } else {
            constraints[0].constant = originalHeight
            
        }
    }
    
    func setRounded(roundedType: RoundedType, color: UIColor) {
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.borderWidth = 1
        self.layer.borderColor = color.cgColor
        self.clipsToBounds = true
    }
    
    func setShadow(){
        var bounds:CGRect = self.bounds
        if self.isKind(of: UITableView.classForCoder()){
            let headerHeight = (self as! UITableView).headerView(forSection: 0)?.frame.height ?? 0
            bounds = CGRect(x: self.bounds.origin.x, y: self.bounds.origin.y, width: self.bounds.size.width, height: (self as! UITableView).contentSize.height - headerHeight)
        }
        
        let shadowPath = UIBezierPath(rect: bounds)
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        layer.shadowOpacity = 0.5
        layer.shadowPath = shadowPath.cgPath
    }
    
    func setCornerRadius(color: UIColor, borderWidth: CGFloat) {
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = color.cgColor
        
    }
    
    func didSelect(completion: @escaping(Bool?) -> Void) {
        UIView.animate(withDuration: 0.1, animations: {
            self.alpha = 0
            
        }, completion: { _ in
            UIView.animate(withDuration: 0.05, animations: {
                self.alpha = 1
                
            })
            completion(true)
            
        })
    }
    
    func didSelectCell(color: UIColor, success: Bool, completion: @escaping(Bool?) -> Void) {
        let originalColor: UIColor? = self.backgroundColor
        UIView.animate(withDuration: 0.5, animations: {
            self.backgroundColor = color
            
        }, completion: { _ in
            UIView.animate(withDuration: 0.05, animations: {
                self.backgroundColor = success ? .green : color
                
            })
            completion(true)
            
        })
    }
    
    func gradient(colors: [CGColor]) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.frame.size
        gradientLayer.colors = colors
        var locations = [CGFloat]()
        for i in 0...colors.count-1 {
            locations.append(CGFloat(i) / CGFloat(colors.count-1))
        }
        gradientLayer.locations = locations as [NSNumber]
        self.layer.insertSublayer(gradientLayer, at: 0)
        
    }
    
    func rotate180(completion: @escaping(Bool) -> Void) {
        UIView.animate(withDuration: 0.5, animations: {
            self.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            
        }, completion: { _ in
            completion(true)
            
        })
    }
    
    func animationtrigger(duration: Double) {
        let shapeLayer: CAShapeLayer = CAShapeLayer()
        let center: CGPoint = self.center
        let radius: CGFloat = self.frame.width/2
        let circularPath: UIBezierPath = UIBezierPath(arcCenter: center, radius: radius, startAngle: -CGFloat(Double.pi/2), endAngle: CGFloat(2*Double.pi), clockwise: true)
        shapeLayer.path = circularPath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 10
        shapeLayer.strokeEnd = 0
        self.layer.addSublayer(shapeLayer)
        let basicAnimation: CABasicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = 1
        basicAnimation.duration = duration
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
        
    }
    
    func animationProgress(duration: Double, progress: Double, color: UIColor) {
        let shapeLayer: CAShapeLayer = CAShapeLayer()
        let center: CGPoint = self.center
        let radius: CGFloat = self.frame.width/2
        let circularPath: UIBezierPath = UIBezierPath(arcCenter: center, radius: radius, startAngle: -CGFloat(Double.pi/2), endAngle: CGFloat((progress*2 - 0.5)*Double.pi), clockwise: true)
        shapeLayer.path = circularPath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = 10
        shapeLayer.strokeEnd = 0
        self.layer.addSublayer(shapeLayer)
        let basicAnimation: CABasicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = 1
        basicAnimation.duration = duration
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        basicAnimation.isRemovedOnCompletion = false
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
        
    }
    
    func setImage(imgURL: URL?) {
        if let imageView = self as? UIImageView, let url = imgURL {
            let activityView = UIActivityIndicatorView()
            activityView.style = .whiteLarge
            self.addSubview(activityView)
            activityView.center = self.center
            activityView.startAnimating()
            activityView.hidesWhenStopped = true
            imageView.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { image, error, imageURL, originalData in
                activityView.stopAnimating()
                activityView.removeFromSuperview()
                
            })
        }
    }
    
}
