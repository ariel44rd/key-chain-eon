//
//  UITextFieldExtension.swift
//  Key chain
//
//  Created by Ariel on 10/21/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

enum DateFormat {
    
    case mmddyyyy, mmmddyyyy, spanish, yyyymmdd, yyyyddmm, ddMMyyyy
    
    var dateFormatterGet: DateFormatter {
        let tempDF = DateFormatter()
        switch self {
        case .spanish:
            tempDF.dateFormat = "yyy-MM-dd HH:mm:ss"
            
        default:
            tempDF.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            
        }
        return tempDF
        
    }
    
    var dateFormatterOut: DateFormatter {
        let tempDF = DateFormatter()
        tempDF.dateFormat = self.dateFormatOut
        tempDF.locale = self.locale
        return tempDF
    }
    
    var dateFormatOut: String {
        switch self {
        case .mmddyyyy:
            return "MM-dd-yyyy"
        case .mmmddyyyy:
            return "MMM-dd-yyyy"
        case .spanish:
            return "dd 'de' MMMM 'de' yyyy"
        case .yyyymmdd:
            return "yyyy-MM-dd"
        case .yyyyddmm:
            return "yyyy-dd-MM"
        case .ddMMyyyy:
            return "dd/MM/yyyy"
        }
    }
    
    var locale: Locale {
        switch self {
        case .spanish:
            return Locale(identifier: "es_MX")
        default:
            return Locale(identifier: "en_US")
        }
    }
}

extension UITextField {
    func setDate(date: Date, dateFormat: DateFormat) {
        let text = dateFormat.dateFormatterOut.string(from: date)
        self.text = text
        
    }
}

extension Date {
    static func getDate(from: String) -> Date? {
        return DateFormatter().date(from: from)
        
    }
}
