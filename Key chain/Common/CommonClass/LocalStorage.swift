//
//  LocalStorage.swift
//  Key chain
//
//  Created by Ariel on 10/20/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import Foundation
import ObjectMapper

class LocalStorage {
    
    class func saveObject<T: Mappable>(object: T, key: String) {
        let jsonString = JSONManager.getJsonString(object: object)
        UserDefaults.standard.set(jsonString, forKey: key)
        
    }
    
    class func getObjectWith<T: Mappable>(key: String) -> T? {
        if let jsonString = UserDefaults.standard.string(forKey: key) {
            return JSONManager.getObject(jsonString: jsonString)

        }
        return nil
    }
    
}
