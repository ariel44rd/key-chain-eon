//
//  ProgressActivityView.swift
//  Key chain
//
//  Created by Ariel on 10/20/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

class ProgressActivityView: UIView {
    
    // MARK: Attributes
    var activityProgress: UIActivityIndicatorView!
    public static let instance = ProgressActivityView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.activityProgress = UIActivityIndicatorView(style: .whiteLarge)
        self.addSubview(self.activityProgress)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    // MARK: Methods
    func progressViewWill(appear: Bool) {
        if appear {
            self.alpha = 0
            let controller = AlertManager.instance.getTopViewController()
            controller.view.addSubview(self)
            self.center = controller.view.center
            self.frame = controller.view.bounds
            self.activityProgress.startAnimating()
            self.activityProgress.center = self.center
            UIView.animate(withDuration: 0.5, animations: {
                self.alpha = 1
                
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.addSubview(self.activityProgress)
                self.activityProgress.center = self.center
                self.activityProgress.startAnimating()
                self.alpha = 0
                
            }, completion: { _ in
                self.removeFromSuperview()
                
            })
        }
        self.makeViewWith(features: [.color(UIColor.black.withAlphaComponent(0.8))])
        
    }
    
    func progressViewWillOn(view: UIView,appear: Bool) {
        if appear {
            self.alpha = 0
            view.addSubview(self)
            self.center = view.center
            self.frame = view.bounds
            self.activityProgress.startAnimating()
            self.activityProgress.center = self.center
            UIView.animate(withDuration: 0.5, animations: {
                self.alpha = 1
                
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.addSubview(self.activityProgress)
                self.activityProgress.center = self.center
                self.activityProgress.startAnimating()
                self.alpha = 0
                
            }, completion: { _ in
                self.removeFromSuperview()
                
            })
        }
        self.makeViewWith(features: [.color(UIColor.black.withAlphaComponent(0.4))])
        
    }
}
