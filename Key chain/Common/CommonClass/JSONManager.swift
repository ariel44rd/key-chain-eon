//
//  JSONManager.swift
//  Key chain
//
//  Created by Ariel on 10/20/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import ObjectMapper

public class JSONManager {
    
    init() { }
    
    public class func getObject<T: Mappable>(jsonString: String) -> T? {
        return Mapper<T>().map(JSONString: jsonString)
        
    }
    
    public class func getJsonString<T: Mappable>(object: T) -> String {
        return object.toJSONString()!
        
    }
}
