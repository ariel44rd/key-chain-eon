//
//  AlertManager.swift
//  Key chain
//
//  Created by Ariel on 10/20/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

public class AlertManager {
    
    var alert: UIAlertController!
    var alertStyle: UIAlertController.Style!
    public static var instance: AlertManager = AlertManager()
    
    func showAlert(controller: UIViewController?, alertStyle: UIAlertController.Style, title: String?, message: String?, actions: [UIAlertAction]?) {
        if self.getTopViewController() is UIAlertController {
            debugPrint("AlertControllerWillNotPresent")
            
        } else {
            debugPrint("AlertControllerWillPresent")
            self.alert = UIAlertController(title: title, message: message, preferredStyle: alertStyle)
            if let actions = actions as [UIAlertAction]? {
                actions.forEach({ self.alert.addAction($0) })
                
            } else {
                self.alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: { action in
                    self.alert.dismiss(animated: true, completion: nil)
                    
                }))
            }
            if let controller = controller as UIViewController? {
                controller.present(self.alert, animated: true, completion: nil)
                
            } else {
                self.getTopViewController().present(self.alert, animated: true, completion: nil)
                
            }
        }
    }
    
    func getTopViewController() -> UIViewController{
        var topViewController = UIApplication.shared.delegate!.window!!.rootViewController!
        while (topViewController.presentedViewController != nil) {
            if(topViewController.isKind(of: UINavigationController.classForCoder())){
                topViewController = (topViewController as! UINavigationController).viewControllers.last!
                
            }else{
                topViewController = topViewController.presentedViewController!
                
            }
        }
        return topViewController
        
    }
    
}
