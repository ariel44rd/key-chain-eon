//
//  App.swift
//  Key chain
//
//  Created by Ariel on 10/20/18.
//  Copyright © 2018 EON ignite. All rights reserved.
//

import UIKit

public struct App {
    public struct Urls {
        public static let baseURLREST: String = "http://52.41.179.140:8091/wsDemo/api"
        
    }
    
    public struct Token {
        public static let sigupToken = "vuz60R851ki5n6C9ukDwSocm0yKZnqtu"
        public static let forgotPasword = "/LiX2SY51khyvi0TrYFhR4W4egCy9K8V"
        public static let login = "0h8kaRg51khkStKslw3FQqWrT7OJCAPC"
        
    }
    
    public struct MetaData{
        public static let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        public static let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
    }
    
    public struct UserData {
        public static let userDataKey = "EON.key.chain.user.data"
        
    }
    
    public struct Nibs {
        public static let videCell = "VideoTableViewCell"
        public static let searchBar = "SearchBarView"
        public static let detailViewController = "DetailViewController"
        public static let postTableViewCell = "PostTableViewCell"
        public static let imageTableViewCell = "ImageTableViewCell"
        public static let textTableViewCell = "TextTableViewCell"
        public static let webViewTableViewCell = "WebViewTableViewCell"
        
    }
}
